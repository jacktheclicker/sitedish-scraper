const path = require('path');
const sitedishService = require(path.join(__dirname, 'sitedish-services'));

// make scrape() public available.
exports.scrape = scrape;


/**
 * Load all orders and return an JS (JSON) object.
 *
 * @param {Object} configuration Configuration object with the properties 'username', 'password' and 'verbose' (optional)
 * @return {Promise} A promise that resolves with all the orders as JSON object
 */
function scrape(configuration) {
    return sitedishService.getProducts(configuration);
}

