describe('sitedish-services', function () {

    const fs = require('fs');
    const path = require('path');
    const service = require('./sitedish-services');


    beforeEach(function () {
    });


    it('should parse the html for products', function () {
        const html = fs.readFileSync(path.join(__dirname, '..', 'spec', 'data', 'page.html'), 'utf-8');
        const products = service._parseProducts(html);

        expect(products.length).toBe(100);
        // Steekproef.
        let product = products[products.length - 1];
        expect(product.name).toBe('Kikkoman sojasaus 10ml');
        expect(product.price).toBe(40);
        expect(product.categorie).toBe('Overige');
        expect(product.description).toBe('Extra zakje sojasaus');
        // Steekproef.
        product = products[21];
        expect(product.name).toBe('Kindermenu');
        expect(product.price).toBe(575);
        expect(product.categorie).toBe('Combi box');
        expect(product.description).toBe('Voor de jongsten hebben wij een speciaal kindermenu ontwikkeld om ze met sushi kennis te laten maken. Incl. kleine verrassing! <br><br> 2 yakitory kipspiesjes, 2 stukjes maki komkommer, 2 stukjes maki zalm en een gefrituurde garnaal');
    });


    it('should parse the html for categories', function () {
        const html = fs.readFileSync(path.join(__dirname, '..', 'spec', 'data', 'page.html'), 'utf-8');
        const categories = service._parseCategories(html);

        expect(categories.length).toBe(11);
        // Steekproef.
        let category = categories[0];
        expect(category.category).toBe('Poke bowl');
        category = categories[categories.length - 1];
        expect(category.category).toBe('Overige');
    });


    it('should parse the html for choices', function () {
        const html = fs.readFileSync(path.join(__dirname, '..', 'spec', 'data', 'choice.html'), 'utf-8');
        const choices = service._parseChoices(html);

        expect(choices.length).toBe(2);
        expect(choices[0]).toEqual({id: 39, name: 'Normaal', price: 0});
        expect(choices[1]).toEqual({id: 40, name: 'Groot', price: 300});
    });

});
