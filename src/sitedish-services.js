// 'Request' package to handle the https communication.
// https://www.npmjs.com/package/request
// Execute http requests.
const request = require('request');
// path utility class
const path = require('path');

// Fast, flexible & lean implementation of core jQuery designed specifically for the server.
// https://github.com/cheeriojs/cheerio
const cheerio = require('cheerio');
// Read version info of sitedish-scraper.
const packageJson = require(path.join(__dirname, '..', 'package.json'));

// User agent is based on package name and version and includes the email address.
const USER_AGENT_STRING = `${packageJson.name}/${packageJson.version} (${packageJson.email})`;
const HEADERS_GET = {
    'Host': '',
    'User-Agent': USER_AGENT_STRING,
    'Accept': 'text/html',
    'Accept-Language': 'en-US'
};

const debugPrefix = '\x1B[36mDEBUG\x1B[0m: ';


/**
 * Load all categories and products and return an JS (JSON) object.
 *
 * @param {Object} configuration Configuration object with the properties 'verbose' (optional)
 * @return {Promise} A promise that resolves with all the products
 */
function getProducts(configuration) {

    const VERBOSE = !!configuration.verbose;

    const URL_MAIN = configuration.url;
    const URL_CHOICE = `${URL_MAIN}/extraopties.php?productId=`;

    // Update HOST in headers object.
    HEADERS_GET['Host'] = URL_MAIN.replace(/https?:\/\//gi, '');

    return new Promise((resolveFn, rejectFn) => {

        // Load the restaurant page html.
        request.get({url: URL_MAIN, headers: HEADERS_GET}, (error, response, html) => {

            if (VERBOSE) {
                console.log(`${debugPrefix}First GET cookie response: ` + JSON.stringify(response.headers['set-cookie']));
            }

            // Error.
            if (error) {
                rejectFn('Accessing url ' + URL_MAIN + ' to start session failed: ' + JSON.stringify(error));
                return;
            }

            if (response.statusCode !== 200) {
                rejectFn('Accessing url ' + URL_MAIN + ' to start session failed with status code: ' + response.statusCode);
                return;
            }

            //const html = fs.readFileSync(path.join(__dirname, '..', 'spec', 'data', 'page.html'), 'utf-8');

            const categories = parseCategories(html);
            const products = parseProducts(html);

            // Add choices to products.
            updateWithChoices(URL_CHOICE, VERBOSE, products)
                .then(() =>
                    // Return the results.
                    resolveFn({
                        categories,
                        products
                    })
                );

        });
    });
}


/**
 * @private
 *
 * Parse given html and return list of products.
 *
 * @param {string} html page of products
 * @return {array} Array of products
 */
function parseProducts(html) {

    // <tbody itemscope itemtype="http://schema.org/Product">
    // <tr>
    //   <td class="gr-nr" itemprop="productID">1</td>
    //   <td class="gr-ger" colspan="2">
    //     <a class="pc-invoeren keuzes" href="postcode.php" rel="nofollow"
    //             data-productNaam="Poké Bowl kip yakitori"
    //             data-productCategorie="Poke bowl"
    //             data-productId="1"
    //             data-prijs="7.15"
    //             data-aantal="1">
    //       <span class="naam_gerecht" itemprop="name">Poké Bowl kip yakitori</span>
    //     </a>
    //     <!--einde naam, begin omschrijving-->

    //     <span class="oms_gerecht" itemprop="description">Kip op Japanse wijze, edamame, komkommer, wakame en sushirijst</span>
    //     <!--einde omschrijving,begin icons-->
    //     <!--einde icons,begin keuzes-->
    //     <span class="menu_keuze">Keuze uit: Normaal, Groot</span><!--einde keuzes--><td class="gr-pr" itemprop="offers" itemscope itemtype="http://schema.org/Offer"><span itemprop="price">&euro; 7.15</span></td><td class="gr-add"><a class="pc-invoeren keuzes" href="postcode.php" rel="nofollow" data-productNaam="Poké Bowl kip yakitori" data-productCategorie="Poke bowl" data-productId="1" data-prijs="7.15" data-aantal="1"><img src="/algemeen/img/sprite0.png" class="sprite gr-img-add" alt="Toevoegen" title="Toevoegen" /></a></td></tr>
    //     </tbody>

    const products = [];

    const $ = cheerio.load(html);

    // Ignore the popular dishes section.
    $('.popi-ger').remove();

    $('tbody[itemtype="http://schema.org/Product"]').each((index, value) => {

        let $productRow = $(value);
        let $productData = $productRow.find('[data-productId]');
        let product = {};

        product.id = $productData.data('productid');
        product.number = $productRow.find('[itemprop="productID"]').text().trim();
        product.name = $productData.data('productnaam');
        // Price in cents.
        product.price = ($productData.data('prijs') + '').replace('.', '') * 1;
        product.categorie = $productData.data('productcategorie');
        product.description = trimExcessiveWhitespace($productData.parent().find('[itemprop="description"]').html());

        // <span class="menu_keuze">Keuze uit: 4 stuks, 8 stuks</span>
        let choicesText = trimExcessiveWhitespace($productRow.find('.menu_keuze').text());
        if (choicesText) {
            product.choices = choicesText.replace(/Keuze uit: */gi, '').split(/, */);
        }

        products.push(product);
    });

    return products;
}


/**
 * @private
 *
 * Parse given html and return category names and descriptions.
 *
 * @param {string} html page of products
 * @return {array} Array of categories
 */
function parseCategories(html) {

    // <div class="categorie">
    //     <a href="#balk"><img src="/algemeen/img/sprite0.png" class="sprite omhoog scrollto" alt="Omhoog" title="Omhoog" data-target="balk" /></a>
    //     <h4 id="categorie0">Poke bowl</h4>
    // <p><span>Poké (spreek uit als pokay) is een traditioneel gerecht uit Hawaï, waar verse vis en groente centraal staat. Een Poké bowl bestaat uit vis of vlees, gemarineerd in een kom met sushirijst, groente en eventueel andere toppings zoals zeewier. Poké bowl is de populaire culinaire trend van dit moment!</span></p>
    // </div>

    const categories = [];

    const $ = cheerio.load(html);

    // Ignore the popular dishes section.
    $('.popi-ger').remove();

    $('div.categorie').each((index, value) => {

        let $categoryElement = $(value);
        let category = $categoryElement.find('h4').text().trim();
        let categoryDescription = $categoryElement.find('p').text();

        categories.push({
            category,
            categoryDescription
        });
    });

    return categories;
}


/**
 * @private
 *
 * Parse given html and return details about possible choices.
 *
 * @param {string} html page of products
 * @return {array} Choices
 */
function parseChoices(html) {

    // <option class="vreterijOptie" data-id="39" data-naam="Normaal" data-prijs="0.00">Normaal</option>
    // <option class="vreterijOptie" data-id="40" data-naam="Groot" data-prijs="3.00">Groot (+ &euro; 3.00)</option>

    const choices = [];

    const $ = cheerio.load(html);

    $('option.vreterijOptie').each((index, value) => {

        let $categoryElement = $(value);
        let id = $categoryElement.data('id');
        let name = $categoryElement.data('naam');
        let price = ($categoryElement.data('prijs') + '').replace('.', '') * 1;

        choices.push({id, name, price});
    });

    return choices;
}


/**
 * @private
 *
 * Update given array with product choices.
 *
 * @param {string} URL_CHOICE URL for retrieving the choice options
 * @param {boolean} VERBOSE Show logging when true
 * @param {array} products List of products
 * @return {Promise} promise which returns when all calls for choices are done
 */
function updateWithChoices(URL_CHOICE, VERBOSE, products) {

    return new Promise(resolveFn => {

        const promises = [];
        products.forEach(product => {
            if (product.choices) {

                promises.push(new Promise(resolve => {

                    const url = `${URL_CHOICE}${product.id}`;
                    request.get({url, headers: HEADERS_GET}, (error, response, html) => {

                        // Error.
                        if (error) {
                            console.log(`${debugPrefix}Accessing url ${URL_CHOICE} to get choices failed: ${error}`);

                            // We handled the order, that's why we use resolve().
                            resolve();
                            return;
                        }

                        if (VERBOSE) {
                            console.log(`${debugPrefix}Details HTML: ${html}`);
                        }

                        // Parse HTML and update the order with the details.
                        product.choices = parseChoices(html);

                        resolve();
                    });
                }));
            }
        });

        Promise.all(promises).then(resolveFn);
    });
}


/**
 * @private
 *
 * Trim excessive whitespace within the string and trim leading and trailing whitespace completely.
 *
 * @param {String} text String to trim
 * @return {String} Trimmed string or empty string in case the given parameter was not truthy
 */
function trimExcessiveWhitespace(text) {
    return (text || '').replace(/\s+/g, ' ').trim();
}


// Public functions.
exports.getProducts = getProducts;

// For unit testing purposes.
exports._parseProducts = parseProducts;
exports._parseCategories = parseCategories;
exports._parseChoices = parseChoices;
