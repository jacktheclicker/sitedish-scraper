
Sitedish-scraper
================

About
-----
Sitedish-scraper scrapes the SiteDish consumer restaurant website to get all categories and dishes. 
The resulting list is returned in JSON format. 

Developed in collaboration with a Sushi 81. 
Delivery and take-away restaurant of sushi and Poke Bowls. Located in Nijverdal, the Netherlands. 
https://sushi81.nl   

Install
-------
You either install it as local dependency to a project, or install it globally to be able to use it on the command line.

To install <code>sitedish-scraper</code> globally, on the command line type this:

    $ npm install -g sitedish-scraper
        
Once installed globally, you can run it from any location.    

Usage Command Line
------------------
To list all current dishes and categories for example, execute <code>sitedish-scraper</code> with the 
<code>--url</code> option. Optionally add the <code>--verbose</code> flag for extra verbosity of logging.

    $ sitedish-scraper --url <public-website> [--verbose]

    $ sitedish-scraper --url https://bestellen.sushi81.nl
        

For example, the output of the command above could look like this:

    "categories": [
        {
            "category": "Poke bowl",
            "categoryDescription": "Poké (spreek uit als pokay) is een traditioneel gerecht uit Hawaï, waar verse vis en groente centraal staat. Een Poké bowl bestaat uit vis of vlees, gemarineerd in een kom met sushirijst, groente en eventueel andere toppings zoals zeewier. Poké bowl is de populaire culinaire trend van dit moment!"
        }
    ],        
    "products": [
        {
            "id": 1,
            "number": "1",
            "name": "Poké Bowl kip yakitori",
            "price": 715,
            "categorie": "Poke bowl",
            "description": "Kip op Japanse wijze, edamame, komkommer, wakame en sushirijst",
            "choices": [ {
                "id": 45,
                "name": "Normaal",
                "price": 0 
                }, 
                {
                "id": 46,
                "name": "Groot",
                "price": 580
            } ]
        },
        {
            "id": 2,
            "number": "2",
            "name": "Poké Bowl ossenhaas",
            "price": 895,
            "categorie": "Poke bowl",
            "description": "Kort geroosterde ossenhaas in teriyaki, edamame, komkommer, wakame en sushirijst"
        }
    ]

Usage in Code
-------------
We assume npm is installed and the project already contains a package.json file. 
If not, then first initialize the project.

    $ npm init
      
Install <code>sitedish-scraper</code> as node module and save it to package.json:

    $ npm install sitedish-scraper --save

Add <code>sitedish-scraper</code> to your program with <code>require</code> 
and call <code>scrape()</code> with the SiteDish website URL encapsulated within a 
configuration object:  

    const sitedishScraper = require('sitedish-scraper');
    const options = {
        url: 'https://bestellen.sushi81.nl',
        verbose: false
    }
    sitedishScraper.scrape(options)
        .then(orders => {
            // Pretty print orders to console.
            console.log(JSON.stringify({orders: orders}, null, 4));
        })
        .catch(error => {
            console.log('ERROR: Failed to load the orders:');
            console.log(error);
        });

Development
===========

Code style 
----------
JavaScript project code style: 

https://github.com/standard/standard
